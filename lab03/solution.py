def import_graph():

    print("Ime fajla:")
    fajl = str(input())
    # fajl = "lab03/K6.txt"

    f = open(fajl, "r")

    nr_of_vertices = int(f.readline())
    f.readline() # skip the empty line

    matrix = []
    for i in range(nr_of_vertices):
        line  = f.readline()
        matrix.append([])
        for j in line:
            try:
                j = int(j)
            except:
                continue
            matrix[i].append(j)
    
    return matrix, nr_of_vertices

def are_connected(v,v_):
    return matrix[v][v_]


class Vrh:
    def __init__(self, _id, color=None, visited=False, parent=None):
        self.id = _id
        self.color = color
        self.visited = visited
        self.parent = parent


def create_vertices_and_populate_list(nr_of_vertices):
    vertices = []
    for i in range(nr_of_vertices):
        v = Vrh(i)
        vertices.append(v)

    return vertices


def color_vertex(vertex, colors):
    available_colors = []
    for i in colors:
        available_colors.append(i) # make a copy of already used colors
    for i in range(0,nr_of_vertices):
        if are_connected(i, vertex.id) and i != vertex.id:
            if vertices[i].color != None:
                try:
                    available_colors.remove(vertices[i].color) # make the neighbour's color unvailbale
                except ValueError: # the neighbour doesn't yet have a color
                    pass
    if available_colors != []: # preferably pick the colors picked the earliest
        vertex.color = available_colors[0]
    else:
        colors.append(len(colors)) # add a new color
        vertex.color = colors[-1]




def find_chromatic_number(q):

    while q != []:

        curr_vertex = q.pop(0) # dequeue
        for i in range(nr_of_vertices):
            try:
                if curr_vertex.parent.id != i:
                    continue
            except AttributeError: # the vertex does not yet have a parent
                pass
            if are_connected(i,curr_vertex.id):
                i = vertices[i]

                if i.visited:
                    continue
                else:
                    i.visited = True
                    i.parrent = curr_vertex
                    color_vertex(i, colors)
                    q.append(i) # enqueue




# "main" lol

matrix, nr_of_vertices = import_graph()

colors = [0] # list of used colors

vertices = create_vertices_and_populate_list(nr_of_vertices) # intialize the list index is the vertex's position in the graph

for v in vertices:
    if not v.color:

        v.color = colors[0]
        v.visited = True # start from the 0th vertex
        q = [v] # initialize queue

        find_chromatic_number(q) # where the magic happens


result = "Kromatski broj zadanog grafa je: " + str(len(colors))

print(result)
