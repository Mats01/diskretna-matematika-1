holder = []
def niz_rekurzivno_optimal(n, a0, a1, x1, x2):
    w = x1 + x2
    z = -1*x1*x2

    if n == 1:
        return a1
    elif n == 0:
        return a0   
    else:
        nm1 = holder[n-1]
        if holder[n-1] == "empty":
            nm1 = niz_rekurzivno_optimal(n-1, a0, a1, x1, x2)
            holder[n-1] = nm1
        nm2 = holder[n-2]
        if holder[n-2] == "empty":
            nm2 = niz_rekurzivno_optimal(n-2, a0, a1, x1, x2)
            holder[n-2] = nm2
        return w*nm1 + z*nm2




def niz_rekurzivno(n, a0, a1, x1, x2):
    w = x1 + x2
    z = -1*x1*x2

    if n == 1:
        return a1
    elif n == 0:
        return a0   
    else:
        return w*niz_rekurzivno(n-1, a0, a1, x1, x2) + z*niz_rekurzivno(n-2, a0, a1, x1, x2)


def niz_po_formuli(n, a0, a1, x1, x2):
    lambda1 = (a1 - x2*a0)/(x1-x2)
    lambda2 = a0 - lambda1
    print("(%s * %s^%s) + (%s * %s^%s)" % (lambda1, x1, n, lambda2, x2, n))

    return lambda1*pow(x1,n) + lambda2*pow(x2,n) 

print("Unesite prvo rjesenje x_0 karakteristicne jednadzbe:")
x1 = float(input())
print("Unesite drugo rjesenje x_1 karakteristicne jednadzbe:")
x2 = float(input())

print("Unesite vrijednost nultog clana niza a_0:")
a0 = float(input())
print("Unesite vrijednost prvog clana niza a_1:")
a1 = float(input())
print("Unesite redni broj n trazenog clana niza:")
n = int(input())

for i in range(n):
    holder.append("empty")

print("formula: %s" % niz_po_formuli(n, a0, a1, x1, x2))
# print("rekurzivno: %s" % niz_rekurzivno(n, a0, a1, x1, x2))
print("rekurzivno_optimised: %s" % niz_rekurzivno_optimal(n, a0, a1, x1, x2))

