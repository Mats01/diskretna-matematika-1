niz_rekurzivno :: Float -> Float -> Float -> Float -> Int -> Float
niz_rekurzivno a0 a1 x1 x2 n
    | n == 0 = a0
    | n == 1 = a1
    | otherwise  = 
        let w = x1 + x2
            z = -1*x1*x2      
        in (w*niz_rekurzivno a0 a1 x1 x2 (n-1) + z*niz_rekurzivno a0 a1 x1 x2 (n-2))


niz_po_formuli :: Float -> Float -> Float -> Float -> Int -> Float
niz_po_formuli a0 a1 x1 x2 n =
    let lambda1 = (a1 - x2*a0)/(x1-x2)
        lambda2 = a0 - lambda1
    in lambda1*(x1** fromIntegral n) + lambda2*(x2** fromIntegral n)

main = do
    print "Unesite prvo rjesenje x_0 karakteristicne jednadzbe:"
    a <- getLine 
    let x1 = read a :: Float
    
    print "Unesite drugo rjesenje x_1 karakteristicne jednadzbe:"
    a <- getLine 
    let x2 = read a :: Float

    print "Unesite vrijednost nultog clana niza a_0:"
    a <- getLine 
    let a0 = read a :: Float

    print "Unesite vrijednost nultog clana niza a_1:"
    a <- getLine 
    let a1 = read a :: Float

    print "Unesite redni broj n trazenog clana niza:" 
    a <- getLine 
    let n = read a :: Int

    print (niz_rekurzivno a0 a1 x1 x2 n)
    print (niz_po_formuli a0 a1 x1 x2 n)