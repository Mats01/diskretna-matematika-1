print("Ime fajla:")
# fajl = str(input())

f = open("lab02/graf5.txt", "r")

nr_of_vortexes = int(f.readline())
f.readline() # skip the empty line

matrix = []
for i in range(nr_of_vortexes):
    line  = f.readline()
    matrix.append([])
    for j in line:
        try:
            j = int(j)
        except:
            continue
        matrix[i].append(j)

# done importing

def are_connected(v,v_):
    return matrix[v][v_]

min_c = 0

def look_for_path(start, curr, origin, dist):
            
        for i in range(nr_of_vortexes):
            if i == curr or i == origin: 
                continue
            if are_connected(curr, i):
                dist += 1
                if min_c and dist > min_c:
                    return 0
                if i == start:
                    return dist
                
                return look_for_path(start, i, curr, dist)
            

for start in range(nr_of_vortexes):
    new_c = look_for_path(start, start, start, 0)
    
    min_c = new_c if new_c != 0 else min_c

print(min_c)
                


