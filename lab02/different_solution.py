print("Ime fajla:")
fajl = str(input())

f = open(fajl, "r")

nr_of_vortexes = int(f.readline())
f.readline() # skip the empty line

matrix = []
for i in range(nr_of_vortexes):
    line  = f.readline()
    matrix.append([])
    for j in line:
        try:
            j = int(j)
        except:
            continue
        matrix[i].append(j)

# done importing -----------------------


def are_connected(v,v_):
    return matrix[v][v_]
          

def smth(q, start):

    while q != []:

        curr = q.pop(0)
        for i in range(nr_of_vortexes):
            if are_connected(i,curr) and origin[curr] != i:

                if i in visited:
                    return distances[curr] + distances[i] + 1
                else:
                    distances[i] = distances[curr] + 1 

                    visited.append(i)
                    origin[i] = curr
                    q.append(i)
                
                
    return 0



                
min_c = float('inf')

for start in range(nr_of_vortexes):
    q = []
    origin = []
    visited = [start]
    distances = []
    for i in range(nr_of_vortexes):
        origin.append(-1) # iniitalize all vertexes to not have a parent
        distances.append(0)
    q.append(start)
    new_c = smth(q, start)
    if min_c == 3:
        break
    
    min_c = new_c if (new_c > 0 and new_c < min_c) else min_c
    

print(min_c)
                


